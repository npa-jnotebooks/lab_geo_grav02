# LAB_GEO_GRAV02

Jupyter Notebook interattivo per studiare la compensazione isostatica di Airy

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_grav02/HEAD?labpath=UNIMIB_GEO_GRAV02_isostasy.ipynb)
